import router from "./instaPost/routes"
import express from "express"
import bodyParser from "body-parser"


const routes = express.Router();
routes.use(bodyParser.json());
routes.use(bodyParser.urlencoded({ extended: false }));
routes.use("/api/instaPost", router)

export default routes;
