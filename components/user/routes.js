import express from "express";
import bodyParser from "body-parser";
import * as service from "./services";

const router = express.Router();

router.use(bodyParser.json());
router.get("/user", (req, res) => {
  service
    .getByPage(req.query.page || 1, req.query.per_page || 10)
    .then(Users => res.status(200).json({ Users }));
});

router.post("/user", (req, res) => {
  service.createPost(req.body).then(
    users => res.status(200).json(users),
    err => {
      console.error(err);
      res.status(500).send("error");
      return;
    }
  );
});



export default router;