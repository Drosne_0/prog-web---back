import services from "./services"
import express from "express"

export const User = (req, res) => {
    if (!req.body.name) {
        return res.status(400).send({
            message: "user is required"
        });
    } else if (!req.body.img) {
        return res.status(400).send({
            message: "image is required"
        });
    }
    services.createUser(req.body).then(
    User => res.status(200).json(User),
    err => {
        res.status(500).send("error");
        return;
    }
    );
}