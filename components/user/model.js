import mongoose from 'mongoose';

const Schema = mongoose.Schema;

var UserSchema = new Schema({
  img: {
    rel: { type: String, default: "" },
    href: { type: String, default: "" },
  },
  name: String
});

UserSchema.index({ title: 1});
let User = mongoose.model('User', UserSchema);

export default User;
