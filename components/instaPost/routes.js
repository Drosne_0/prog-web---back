import express from "express";
import bodyParser from "body-parser";
import * as service from "./services";

const router = express.Router();

router.use(bodyParser.json());
router.get("/instaPost", (req, res) => {
  service
    .getByPage(req.query.page || 1, req.query.per_page || 10)
    .then(InstaPosts => res.status(200).json({ InstaPosts }));
});

router.post("/instaPost", (req, res) => {
  service.createPost(req.body).then(
    users => res.status(200).json(users),
    err => {
      console.error(err);
      res.status(500).send("error");
      return;
    }
  );
});



export default router;