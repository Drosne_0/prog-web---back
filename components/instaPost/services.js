import InstaPost from "./model";

export async function createPost(instaPost) {
    if (instaPost) {
        return InstaPost.create(instaPost);
    }
}

export async function getByPage(page, per_page) {
    var start = (parseInt(page) - 1) * parseInt(per_page);
    let result = await InstaPost.find({})
      .skip(start)
      .limit(parseInt(per_page));
    return result;
  };
