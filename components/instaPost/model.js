import mongoose from 'mongoose';

const Schema = mongoose.Schema;

var PostSchema = new Schema({
  img: {
    rel: { type: String, default: "" },
    href: { type: String, default: "" },
  },
  user: String,
  date: String,
  description: String,
  nbLikes: Number
});

PostSchema.index({ title: 1});
let InstaPost = mongoose.model('InstaPost', PostSchema);

export default InstaPost;
