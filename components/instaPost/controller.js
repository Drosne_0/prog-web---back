import services from "./services"
import express from "express"

export const InstaPost = (req, res) => {
    if (!req.body.user) {
        return res.status(400).send({
            message: "user is required"
        });
    } else if (!req.body.description) {
        return res.status(400).send({
            message: "description is required"
        });
    } else if (!req.body.date) {
        return res.status(400).send({
            message: "date is required"
        });
    } else if (!req.body.img) {
        return res.status(400).send({
            message: "image is required"
        });
    }
    services.createPost(req.body).then(
    InstaPost => res.status(200).json(InstaPost),
    err => {
        res.status(500).send("error");
        return;
    }
    );
}

